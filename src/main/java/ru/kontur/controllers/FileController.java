package ru.kontur.controllers;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.ServletContextResource;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@RestController
@RequestMapping("/api/files")
public class FileController {
    //root path for image files
    private String FILE_PATH_ROOT = "C:\\Users\\Damir\\IdeaProjects\\kontur_test\\storage\\";

    @Autowired
    private ServletContext servletContext;


    @GetMapping("/{filename}")
    public ResponseEntity<byte[]> getImage(@PathVariable("filename") String filename) {
        byte[] image = new byte[0];
        try {
            image = FileUtils.readFileToByteArray(new File(FILE_PATH_ROOT+filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
    }

    @RequestMapping(value = "/2/{filename}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> getImageAsResource(@PathVariable("filename") String filename) {
        HttpHeaders headers = new HttpHeaders();
        Resource resource =
                new ServletContextResource(servletContext, FILE_PATH_ROOT+filename);
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/3/{filename}", method = RequestMethod.GET)
    public void getImage(HttpServletResponse response, @PathVariable String filename) throws IOException {
        File file = new File(FILE_PATH_ROOT+filename);
        if(file.exists()) {
            String contentType = "application/octet-stream";
            response.setContentType(contentType);
            OutputStream out = response.getOutputStream();
            FileInputStream in = new FileInputStream(file);
            // copy from in to out
            IOUtils.copy(in, out);
            out.close();
            in.close();
        }else {
            throw new FileNotFoundException();
        }
    }

    @PostMapping("/createImage")
    public void createImage(){
        BufferedImage bufferedImage = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
        File file = new File(FILE_PATH_ROOT + "image.bmp");
        File bojack = new File(FILE_PATH_ROOT+"bojack.jpg");

        Graphics2D g2d = bufferedImage.createGraphics();

        BufferedImage bufferedImage2 = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
        g2d.setColor(Color.yellow);
        g2d.drawString("yo", 50, 50);

//        bufferedImage.getGraphics().drawImage(bufferedImage2, 100, 100, null);
        try {
            bufferedImage.getGraphics().drawImage(ImageIO.read(bojack), 400, 400, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Disposes of this graphics context and releases any system resources that it is using.
        g2d.dispose();

        try {
            ImageIO.write(bufferedImage, "bmp", file);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
