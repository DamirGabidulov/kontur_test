package ru.kontur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KonturTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(KonturTaskApplication.class, args);
    }

}
